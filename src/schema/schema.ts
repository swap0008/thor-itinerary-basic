import * as Joi from '@hapi/joi';
import { ItineraryModel } from '../models/ItineraryModel';

const getFields = () => {
    return {
        [ItineraryModel.NAME]: Joi.string(),
        [ItineraryModel.DESCRIPTION]: Joi.string(),
        [ItineraryModel.PRICE]: Joi.number(),
        [ItineraryModel.DURATION]: Joi.string(),
        [ItineraryModel.CURRENCY]: Joi.string(),
        [ItineraryModel.DISCOUNT]: Joi.string(),
        [ItineraryModel.DISCOUNT_TYPE]: Joi.string(),
        [ItineraryModel.PHOTOS]: Joi.array().items(Joi.string().allow('')),
        [ItineraryModel.DAY_IDS]: Joi.array().items(Joi.string().allow('')),
        [ItineraryModel.AMENITIES]: Joi.array().items(Joi.string().allow('')),
        [ItineraryModel.TAGS]: Joi.array().items(Joi.string().allow('')),
        [ItineraryModel.TERMS_AND_CONDITION]: Joi.string(),
        [ItineraryModel.CANCELLATION_POLICY]: Joi.string(),
        [ItineraryModel.TRAVEL_DATE_TYPE]: Joi.string().valid('flexible', 'fixed'),
        [ItineraryModel.DATES]: Joi.array().items(Joi.string().allow('')),
        [ItineraryModel.PRICING_TYPE]: Joi.string(),
        [ItineraryModel.INCLUSIONS]: Joi.string(),
        [ItineraryModel.EXCLUSIONS]: Joi.string(),
        [ItineraryModel.CREATED_BY]: Joi.string(),
        [ItineraryModel.IDENTIFIER]: Joi.string(),
        [ItineraryModel.SLUG]: Joi.string(),
        [ItineraryModel.BUSINESS_ID]: Joi.string()
    };
}

export const saveSchema = (data) => {
    const fields = getFields();
    const schema = Joi.object({
        [ItineraryModel.NAME]: fields[ItineraryModel.NAME].required(),
        [ItineraryModel.DESCRIPTION]: fields[ItineraryModel.DESCRIPTION],
        [ItineraryModel.PRICE]: fields[ItineraryModel.PRICE].required(),
        [ItineraryModel.DURATION]: fields[ItineraryModel.DURATION].required(),
        [ItineraryModel.CURRENCY]: fields[ItineraryModel.CURRENCY].required(),
        [ItineraryModel.DISCOUNT]: fields[ItineraryModel.DISCOUNT].allow(''),
        [ItineraryModel.DISCOUNT_TYPE]: fields[ItineraryModel.DISCOUNT_TYPE].allow(''),
        [ItineraryModel.PHOTOS]: fields[ItineraryModel.PHOTOS],
        [ItineraryModel.DAY_IDS]: fields[ItineraryModel.DAY_IDS],
        [ItineraryModel.AMENITIES]: fields[ItineraryModel.AMENITIES].allow(''),
        [ItineraryModel.TAGS]: fields[ItineraryModel.TAGS].allow(''),
        [ItineraryModel.TERMS_AND_CONDITION]: fields[ItineraryModel.TERMS_AND_CONDITION].required(),
        [ItineraryModel.CANCELLATION_POLICY]: fields[ItineraryModel.CANCELLATION_POLICY].required(),
        [ItineraryModel.TRAVEL_DATE_TYPE]: fields[ItineraryModel.TRAVEL_DATE_TYPE].required(),
        [ItineraryModel.DATES]: fields[ItineraryModel.DATES],
        [ItineraryModel.PRICING_TYPE]: fields[ItineraryModel.PRICING_TYPE].allow(''),
        [ItineraryModel.INCLUSIONS]: fields[ItineraryModel.INCLUSIONS].allow(''),
        [ItineraryModel.EXCLUSIONS]: fields[ItineraryModel.EXCLUSIONS].allow(''),
        [ItineraryModel.CREATED_BY]: fields[ItineraryModel.CREATED_BY].required(),
        [ItineraryModel.IDENTIFIER]: fields[ItineraryModel.IDENTIFIER].required(),
        [ItineraryModel.SLUG]: fields[ItineraryModel.SLUG].allow(''),
        [ItineraryModel.BUSINESS_ID]: fields[ItineraryModel.BUSINESS_ID].required()
    });

    return schema.validate(data);
}

export const updateSchema = (data) => {
    const fields = getFields();
    const schema = Joi.object({
        [ItineraryModel.NAME]: fields[ItineraryModel.NAME],
        [ItineraryModel.DESCRIPTION]: fields[ItineraryModel.DESCRIPTION],
        [ItineraryModel.PRICE]: fields[ItineraryModel.PRICE],
        [ItineraryModel.DURATION]: fields[ItineraryModel.DURATION],
        [ItineraryModel.CURRENCY]: fields[ItineraryModel.CURRENCY],
        [ItineraryModel.DISCOUNT]: fields[ItineraryModel.DISCOUNT].allow(''),
        [ItineraryModel.DISCOUNT_TYPE]: fields[ItineraryModel.DISCOUNT_TYPE].allow(''),
        [ItineraryModel.PHOTOS]: fields[ItineraryModel.PHOTOS],
        [ItineraryModel.DAY_IDS]: fields[ItineraryModel.DAY_IDS],
        [ItineraryModel.AMENITIES]: fields[ItineraryModel.AMENITIES].allow(''),
        [ItineraryModel.TAGS]: fields[ItineraryModel.TAGS].allow(''),
        [ItineraryModel.TERMS_AND_CONDITION]: fields[ItineraryModel.TERMS_AND_CONDITION],
        [ItineraryModel.CANCELLATION_POLICY]: fields[ItineraryModel.CANCELLATION_POLICY],
        [ItineraryModel.TRAVEL_DATE_TYPE]: fields[ItineraryModel.TRAVEL_DATE_TYPE],
        [ItineraryModel.DATES]: fields[ItineraryModel.DATES],
        [ItineraryModel.PRICING_TYPE]: fields[ItineraryModel.PRICING_TYPE].allow(''),
        [ItineraryModel.INCLUSIONS]: fields[ItineraryModel.INCLUSIONS].allow(''),
        [ItineraryModel.EXCLUSIONS]: fields[ItineraryModel.EXCLUSIONS].allow(''),
        [ItineraryModel.CREATED_BY]: fields[ItineraryModel.CREATED_BY],
        [ItineraryModel.IDENTIFIER]: fields[ItineraryModel.IDENTIFIER],
        [ItineraryModel.SLUG]: fields[ItineraryModel.SLUG].allow('')
    });

    return schema.validate(data);
}