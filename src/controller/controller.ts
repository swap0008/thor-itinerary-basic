import { pick } from 'lodash';
import * as aws from 'aws-sdk';
import { v4 as uuidv4 } from 'uuid';
import { ItineraryModel } from '../models/ItineraryModel';
import { DayModel } from '../models/DayModel';
import { saveSchema, updateSchema } from '../schema/schema';
import {
    CAN_READ_ITINERARY_BASIC,
    CAN_CREATE_ITINERARY_BASIC,
    CAN_DELETE_ITINERARY_BASIC,
    CAN_UPDATE_ITINERARY_BASIC
} from '../constants/constants';

const Itinerary = new ItineraryModel();
const Day = new DayModel();

aws.config.update({
    secretAccessKey: 'zBuvhwvdC/RHhbCmXiSx12EEB15OrTnda+GFBixL',
    accessKeyId: 'AKIAUW5RDVCWHKUTGQOS',
    region: 'ap-south-1'
})

const s3 = new aws.S3();

export const getSignedUrlController = (event, callback) => {
    const { name, type } = event.body;

    const s3Params = {
        Bucket: 'itinerary-photos',
        Key: name,
        ContentType: type,
        ACL: 'public-read',
    };

    const uploadURL = s3.getSignedUrl('putObject', s3Params);
    response(callback, 200, { uploadURL });
}

const response = (callback, statusCode, body) => {
    return callback(null, {
        statusCode,
        body: JSON.stringify(body),
        headers: {
            "Access-Control-Allow-Origin": "*"
        }
    });
}

const createSlug = (name) => {
    const currentTime = new Date().getTime()
    const slug = name.toLowerCase().replace(/ /g, '-').replace(/\'|\"|/g, '') + '-' + currentTime;
    return slug;
}

const filterDataUsingFields = (fields, data) => {
    for (const field of Object.keys(data)) {
        if (!fields[field]) delete data[field];
    }

    return data;
}

export const getItineraryController = async (event, callback) => {
    const { identifier, permissions } = event;
    if (!permissions[CAN_READ_ITINERARY_BASIC]) return response(callback, 403, 'Unauthorized!');

    const { business_id, slug } = event.pathParameters || {};
    let { embeds, fields = '' } = event.queryStringParameters || {};

    const fieldsObject = {};
    fields.split(',').forEach(field => fieldsObject[field] = true);

    if (!slug) {
        const itinerariesDocs = await Itinerary.getItinerariesByIdentifierAndBusinessId(identifier, business_id);
        const itineraries = {};
        for (const doc of itinerariesDocs.docs) {
            let data = doc.data();
            if (fields.length) {
                data = filterDataUsingFields(fieldsObject, data);
            }
            itineraries[doc.id] = { id: doc.id, ...data }
        }

        return callback(null, { body: JSON.stringify(itineraries) });
    }

    if (!embeds) {
        const itinerary = await Itinerary.getItineraryByIdentifierAndBusinessIdAndSlug(identifier, business_id, slug);
        if (itinerary.empty) return response(callback, 400, 'No itinerary found.');

        const result = itinerary.docs[0];
        const responseBody = result.data();
        responseBody.id = result.id;
        
        return response(callback, 200, responseBody);
    }

    if (embeds === 'days_data') {
        const daysDoc = await Day.getDaysByItineraryId(slug);
        const days = daysDoc.docs.map(doc => doc.data());
        return response(callback, 200, days);
    }

    response(callback, 400, 'Bad Request.');
}

export const saveItineraryController = async (event, callback) => {
    const { identifier, user_id, permissions } = event;
    if (!permissions[CAN_CREATE_ITINERARY_BASIC]) return response(callback, 403, 'Unauthorized!');

    const data = pick(event.body, [
        ItineraryModel.NAME,
        ItineraryModel.DESCRIPTION,
        ItineraryModel.PRICE,
        ItineraryModel.DURATION,
        ItineraryModel.CURRENCY,
        ItineraryModel.DISCOUNT,
        ItineraryModel.DISCOUNT_TYPE,
        ItineraryModel.PHOTOS,
        ItineraryModel.DAY_IDS,
        ItineraryModel.AMENITIES,
        ItineraryModel.TAGS,
        ItineraryModel.TERMS_AND_CONDITION,
        ItineraryModel.CANCELLATION_POLICY,
        ItineraryModel.TRAVEL_DATE_TYPE,
        ItineraryModel.INCLUSIONS,
        ItineraryModel.EXCLUSIONS,
        ItineraryModel.BUSINESS_ID,
        ItineraryModel.HOTEL_DETAILS,
        ItineraryModel.AMENITIES,
        ItineraryModel.EXCLUSIONS_NOTES,
        ItineraryModel.INCLUSIONS_NOTES,
        ItineraryModel.TAGS
    ]);

    data[ItineraryModel.IDENTIFIER] = identifier;
    data[ItineraryModel.CREATED_BY] = user_id;

    // const { error } = saveSchema(data);
    // if (error) return response(callback, 400, error.details[0].message);

    data[ItineraryModel.SLUG] = createSlug(data[ItineraryModel.NAME]);

    console.log('working');
    
    const batch = Day.createBatch();
    for (const day of event.body.days) {
        const docRef = Day.collectionRef.doc();
        data[ItineraryModel.DAY_IDS].push(docRef.id);

        batch.set(docRef, { [DayModel.DESCRIPTION]: day });
    }

    await batch.commit();

    console.log(data);


    const { id } = Itinerary.collectionRef.doc();
    await Itinerary.set(id, data);

    data.id = id;
    response(callback, 200, data);
}

export const updateItineraryController = async (event, callback) => {
    // const { business_id } = event.pathParameters || {};
    const { permissions } = event;
    if (!permissions[CAN_UPDATE_ITINERARY_BASIC]) return response(callback, 403, 'Unauthorized!');

    const { itinerary_id } = event.body;

    const data = pick(event.body, [
        ItineraryModel.NAME,
        ItineraryModel.DESCRIPTION,
        ItineraryModel.PRICE,
        ItineraryModel.DURATION,
        ItineraryModel.CURRENCY,
        ItineraryModel.DISCOUNT,
        ItineraryModel.DISCOUNT_TYPE,
        ItineraryModel.PHOTOS,
        ItineraryModel.DAY_IDS,
        ItineraryModel.AMENITIES,
        ItineraryModel.TAGS,
        ItineraryModel.TERMS_AND_CONDITION,
        ItineraryModel.CANCELLATION_POLICY,
        ItineraryModel.TRAVEL_DATE_TYPE,
        ItineraryModel.INCLUSIONS,
        ItineraryModel.EXCLUSIONS
    ]);

    const { error } = updateSchema(data);
    if (error) return response(callback, 400, error.details[0].message);

    //create slug if name changed
    if (data[ItineraryModel.NAME])
        data[ItineraryModel.SLUG] = createSlug(data[ItineraryModel.NAME]);

    await Itinerary.update(itinerary_id, data);

    data['id'] = itinerary_id;
    response(callback, 200, data);
}

export const deleteItineraryController = async (event, callback) => {
    const { permissions } = event;

    if (!permissions[CAN_DELETE_ITINERARY_BASIC]) return response(callback, 403, 'Unauthorized!');

    const { business_id } = event.pathParameters || {};
    const { itinerary_id } = event.body;

    if (!itinerary_id) return response(callback, 400, 'Itinerary ID required');

    //delete all day belongs to itinerary
    const days = await Day.getDaysByItineraryId(itinerary_id);
    const batch = Day.createBatch();

    if (!days.empty) {
        for (const day of days.docs) {
            batch.delete(day.ref);
        }
        await batch.commit();
    }

    await Itinerary.delete(itinerary_id);
    response(callback, 200, { itineraryId: itinerary_id });
}

// export const searchPlaces = async (event, callback) => {
//     try {
//         // const places = await googleMapsClient.placesAutoComplete({ input: 'Taj Hotel', sessiontoken: '24asdg5' }).asPromise();
//         const places = await googleMapsClient.places({ query: 'Universal studios singapore' }).asPromise();
//         callback(null, { body: JSON.stringify(places) });
//     } catch (e) {
//         callback(null, { body: JSON.stringify(e) });
//     }
// }

// export const getPhotos = async (event, callback) => {
//     // const { photoRef } = JSON.parse(event.body);
//     try {
//         const photos = await googleMapsClient.placesPhoto({
//             maxwidth: 400,
//             photoreference: 'CmRaAAAAKfKGm7F-BzfaxpKajlUn1cG_UoOJHmRs4aAqwgIp-F2rMJjrlnJSl6BW3DOIUbw0CV3X1IVHvZoeyeQXlmWYjdVDpvblIOGLS1yHzp1cZokAhixD1YRe_bBHfyF1wi79EhCHOqnCT8DCJRGlKZ18EvYQGhTAcxdzpyqmKOQgVf0yMNGC-41TUw'
//         }).asPromise();
//         callback(null, { body: JSON.stringify(photos) });
//     } catch (e) {
//         callback(null, { body: JSON.stringify(e) });
//     }
// }