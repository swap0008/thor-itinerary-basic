import { FirestoreClient } from './FirestoreClient.js';

export class DayModel extends FirestoreClient {
    static ITINERARY_ID = 'itinerary_id';
    static DAY = 'day';
    static DESCRIPTION = 'description';
    static EVENTS = 'events';
    
    constructor() {
        super('days');
    }

    getDaysByItineraryId = (itinerary_id) => {
        const dayDocs = this.collectionRef.where(DayModel.ITINERARY_ID, '==', itinerary_id).get();
        return dayDocs;
    }
}