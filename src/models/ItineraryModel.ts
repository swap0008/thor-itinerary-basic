import { FirestoreClient } from './FirestoreClient.js';

export class ItineraryModel extends FirestoreClient {
    static NAME = 'name';
    static DESCRIPTION = 'description';
    static PRICE = 'price';
    static DURATION = 'duration';
    static CURRENCY = 'currency';
    static DISCOUNT = 'discount';
    static DISCOUNT_TYPE = 'discount_type';
    static PHOTOS = 'photos';
    static DAY_IDS = 'days';
    static AMENITIES = 'amenities';
    static TAGS = 'tags';
    static TERMS_AND_CONDITION = 'terms_and_condition';
    static CANCELLATION_POLICY = 'cancellation_policy';
    static TRAVEL_DATE_TYPE = 'travel_date_type';
    static DATES = 'dates';
    static PRICING_TYPE = 'pricing_type';
    static INCLUSIONS = 'inclusions';
    static EXCLUSIONS = 'exclusions';
    static CREATED_BY = 'created_by';
    static IDENTIFIER = 'identifier';
    static SLUG = 'slug';
    static BUSINESS_ID = 'business_id';
    static OVERVIEW = 'overview';
    static DESTINATIONS = 'destinations';
    static INCLUSIONS_NOTES = 'inclusions_notes';
    static EXCLUSIONS_NOTES = 'exclusions_notes';
    static HOTEL_DETAILS = 'hotel_details';

    constructor() {
        super('itineraries_basic');
    }

    getItinerariesByIdentifierAndBusinessId = (identifier: string, business_id: string) => {
        return this.collectionRef
            .where(ItineraryModel.IDENTIFIER, '==', identifier)
            .where(ItineraryModel.BUSINESS_ID, '==', business_id).get();
    }

    getItineraryByIdentifierAndBusinessIdAndSlug = (identifier: string, business_id: string, slug: string) => {
        return this.collectionRef
            .where(ItineraryModel.IDENTIFIER, '==', identifier)
            .where(ItineraryModel.BUSINESS_ID, '==', business_id)
            .where(ItineraryModel.SLUG, '==', slug).get();
    }
}