import * as admin from 'firebase-admin';
import { pick } from 'lodash';
// import { serviceAccount } from '../ServiceAccount';

const serviceAccount: object = pick(process.env, [
	'type',
	'project_id',
	'private_key_id',
	'private_key',
	'client_email',
	'client_id',
	'auth_uri',
	'token_uri',
	'auth_provider_x509_cert_url',
	'client_x509_cert_url'
]);

if (!process.env.abc) {
	admin.initializeApp({
		credential: admin.credential.cert(serviceAccount)
	});
	process.env.abc = 'asdg';
}


const db = admin.firestore();

export class FirestoreClient {
	collectionRef: any;
	schema: any;

	constructor(collectionName: any, schema = {}) {
		this.collectionRef = db.collection(collectionName);
		this.schema = schema;
	}

	createBatch = () => {
		return db.batch();
	}

	getDoc = (id: string) => {
		return this.collectionRef.doc(id).get();
	};

	get = () => {
		return this.collectionRef.get();
	};

	set = async (id: string, data: any) => {
		// const { error = false } = this.schema.saveSchema(data);
		// if (error) return error.details[0].message;

		return await this.collectionRef.doc(id).set(data);
	};

	add = async (data: any) => {
		return await this.collectionRef.add(data);
	};

	update = async (id: string, data: any) => {
		// const { error = false } = this.schema.updateSchema(data);
		// if (error) return error.details[0].message;

		return await this.collectionRef.doc(id).update(data);
	};

	updateArrayUnion = async (id: string, newData: any) => {
		const { error } = this.schema.updateSchema(newData);
		if (error) return error.details[0].message;

		const ref = await this.collectionRef.doc(id);
		const transaction = await db.runTransaction(async (t) => {
			let doc: any = await t.get(ref);
			let oldData = doc.data();
			Object.keys(newData).forEach((key) => (newData[key] = [...new Set(oldData[key].concat(newData[key]))]));
			const result = await t.update(ref, newData);
			return Promise.resolve(newData);
		});

		return transaction;
	};

	updateArrayRemove = async (id: string, field: string, value: any) => {
		return await this.collectionRef.doc(id).update({
			[field]: admin.firestore.FieldValue.arrayRemove(value)
		});
	};

	incrementValue = async (id: string, field: any) => {
		return await this.collectionRef.doc(id).update({
			[field]: admin.firestore.FieldValue.increment(1)
		});
	};

	decrementValue = async (id: string, field: any) => {
		return await this.collectionRef.doc(id).update({
			[field]: admin.firestore.FieldValue.increment(-1)
		});
	};

	delete = async (id: any) => {
		return await this.collectionRef.doc(id).delete();
	};

	multipleDocQuery = async (ids: any) => {
		const result = await this.collectionRef.where('day_id', '==', ids).get();
		const data: object[] = [];
		result.docs.map((doc: any) => data.push(doc.data()));
		return data;
	}

	deleteMultipleDoc = async (ids: any, snapshot: any) => {
		if (!ids.length) return { message: 'ids length empty' };
		if (!snapshot)
			snapshot = await this.collectionRef.where('id', 'in', ids).get();
		const batch = db.batch();
		snapshot.forEach(doc => batch.delete(doc.ref));
		const result = await batch.commit();
		return result;
	}
}