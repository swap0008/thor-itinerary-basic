// import * as jwt from 'jsonwebtoken';
// import { CAN_ACCESS_CONTACTS, PLATFORM_CONTACTS_LIMIT } from '../constants/constants.js';

// export const verifyJwtToken = (req, res, next) => {
//     const token = req.header('x-auth-token');
//     if (!token) return res.status(401).send('Access denied. No token provided');

//     const { permissions, limits } = jwt.verify(token, process.env['jwt_private_key']);

//     req.permissions = permissions;
//     req.limits = limits;

//     next();
// }

// export const checkPerimissions = (req, res, next) => {
//     const { permissions, limits } = req;
//     const { total_platform_contacts } = req.body;

//     if (!permissions[CAN_ACCESS_CONTACTS])
//         return res.status(400).send('Access denied. You are not authorized to access contacts api.');

//     if (total_platform_contacts && total_platform_contacts >= limits[PLATFORM_CONTACTS_LIMIT])
//         return res.status(400).send('Limit exceeded.');

//     next();
// }