import {
    getItineraryController,
    saveItineraryController,
    updateItineraryController,
    deleteItineraryController,
    getSignedUrlController
} from '../controller/controller.js';
import {
    applyMiddleware,
    verifyJwtToken,
    verifyBusiness
} from 'middlewares-nodejs';

export const getItinerary = (event, context, callback) => {
    applyMiddleware(event, callback, [verifyJwtToken, verifyBusiness], getItineraryController);
}

export const saveItinerary = (event, context, callback) => {
    applyMiddleware(event, callback, [verifyJwtToken, verifyBusiness], saveItineraryController);
}

export const deleteItinerary = (event, context, callback) => {
    applyMiddleware(event, callback, [verifyJwtToken, verifyBusiness], deleteItineraryController);
}

export const updateItinerary = (event, context, callback) => {
    applyMiddleware(event, callback, [verifyJwtToken, verifyBusiness], updateItineraryController);
}

export const healthCheck = (event, context, callback) => {
    callback(null, { body: `You're being served from ap-south-1 Mumbai region (ItineraryBasicAPI)` });
}

export const getSignedUrl = (event, context, callback) => {
    applyMiddleware(event, callback, [verifyBusiness], getSignedUrlController);
}

// export const getPlaces = (event, context, callback) => {
//     searchPlaces(event, callback);
// }

// export const getPhotoss = (event, context, callback) => {
//     getPhotos(event, callback);
// }